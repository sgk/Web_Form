#!/usr/bin/env python                                                                                                                   

from flask import Flask, render_template, request# redirect, url_for

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('form.html')


@app.route('/welcome/', methods=['GET','POST'])
def welcome():
    if request.method == 'POST':
        name = request.form['yourname']
        email = request.form['youremail']
        domain = request.form['domain']
        print name, email, domain
        return render_template('welcome.html',name=name)
    return render_template('welcome.html', name= "anonymus")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

